package ru.geekbrains;

import java.util.ArrayList;

/**
 * Выполнил Александр Синий
 */
public class Main {

    /**
     * Задача 2
     * Реализовать функцию возведения числа a в степень b:
     * a. без рекурсии;
     * b. рекурсивно;
     * c. *рекурсивно, используя свойство чётности степени.
     */
    public static void pow() {
        // a. без рекурсии
        powViaCycle(2, 8);
        // b. рекурсивно
        powViaRecursion(-2, -2);
        // c. *рекурсивно, используя свойство чётности степени
        fastPowViaRecursion(2, -1);
    }

    private static double powViaCycle(int a, int b) {
        if (b == 0) return 1;
        int i = (b > 0) ? b : -b;
        double result = 1d;
        while (i > 0) {
            result *= a;
            i--;
        }
        if (b < 0) return 1d / result;
        return result;
    }

    private static double powViaRecursion(int a, int b) {
        if (b == 0) return 1;
        if (b > 0) return a * powViaRecursion(a, --b);
        return (1d / a) * powViaRecursion(a, ++b);
    }

    // x^(2*m) = (x^m)^2
    private static double fastPowViaRecursion(double a, double b) {
        if (b == 0) return 1;
        if (b == 1) return a;
        if (b == -1) return 1d / a;
        if (b == 2) return a * a;
        if (b == -2) return 1d / (a * a);
        if (b % 2 == 0) return fastPowViaRecursion(fastPowViaRecursion(a, b / 2), 2);
        return a * fastPowViaRecursion(a, --b);
    }

    /**
     * Задача 3
     * **Исполнитель «Калькулятор» преобразует целое число, записанное на экране.
     * У исполнителя две команды, каждой присвоен номер:
     * 1. Прибавь 1.
     * 2. Умножь на 2.
     * Первая команда увеличивает число на экране на 1, вторая увеличивает его в 2 раза.
     * Сколько существует программ, которые число 3 преобразуют в число 20:
     * а. С использованием массива.
     * b. *С использованием рекурсии.
     */
    public static void calculator() {
        // а. С использованием массива
        convertViaArray(3, 20);
        // b. *С использованием рекурсии
        convertViaRecursion(3, 20);
    }

    public static int convertViaArray(int a, int b) {
        if (a > b) return 0;
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(a);
        boolean arrHasNumLessB = true;
        while (arrHasNumLessB) {
            arrHasNumLessB = false;
            for(int i = 0; i < arr.size(); i++) {
                final int c = arr.get(i);
                if (c == b) continue;
                arrHasNumLessB = true;
                if (c + 1 > b) continue;
                arr.set(i, c + 1);
                if (c * 2 > b) continue;
                arr.add(c * 2);
            }
        }
        return arr.size();
    }

    public static int convertViaRecursion(int a, int b) {
        if (a > b) return 0;
        if (a == b) return 1;
        return convertViaRecursion(a + 1, b) + convertViaRecursion(a * 2, b);
    }
}
